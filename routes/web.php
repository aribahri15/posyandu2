<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PosyanduController;
use App\Http\Controllers\ChildController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ReportVaksinVitaminController;
use App\Http\Controllers\ReportKmsController;
use App\Http\Controllers\ParentController;

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('/user', [UserController::class, 'index']);
    Route::post('/user', [UserController::class, 'store'])->name('user.store');
    Route::post('/user/update', [UserController::class, 'update'])->name('user.update');

    Route::get('/posyandu', [PosyanduController::class, 'index']);
    Route::post('/posyandu', [PosyanduController::class, 'create'])->name('posyandu.create');
    Route::post('/posyandu/update', [PosyanduController::class, 'update'])->name('posyandu.update');
    // Route::get('/posyandu/delete/{id}', [PosyanduController::class, 'delete'])->name('posyandu.delete');

    Route::get('/posyandu/provinces', [PosyanduController::class, 'provinces']);
    Route::get('/posyandu/cities', [PosyanduController::class, 'cities']);
    Route::get('/posyandu/districts', [PosyanduController::class, 'districts']);
    Route::get('/posyandu/villages', [PosyanduController::class, 'villages']);
    
    Route::get('/parent', [ParentController::class, 'index']);
    Route::get('/parent/create', [ParentController::class, 'create']);
    Route::post('/parent', [ParentController::class, 'store'])->name('parent.store');
    Route::get('/parent/{id}', [ParentController::class, 'detail']);
    
    Route::get('/child', [ChildController::class, 'index']);
    Route::get('/child/create', [ChildController::class, 'create']);
    Route::get('/child/delete/{id}', [ChildController::class, 'delete']);
    Route::post('/child', [ChildController::class, 'store'])->name('child.store');
    Route::get('/child/{id}', [ChildController::class, 'show']);

    Route::post('/kms/{id}', [ReportKmsController::class, 'store'])->name('kms.store');
    Route::post('/vitamin/{id}', [ReportVaksinVitaminController::class, 'store'])->name('vitamin.store');

    // Route::get('/child', function () {
    //     return view('child.index');
    // });
    // Route::get('/child/create', function () {
    //     return view('child.create');
    // });
    // Route::get('/child/1', function () {
    //     return view('child.detail');
    // });

    Route::get('/setting', function () {
        return view('setting.index');
    });

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

});
