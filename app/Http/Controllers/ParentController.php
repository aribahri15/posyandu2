<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parents;
use App\Models\Child;
use App\Models\Posyandu;
use Indonesia;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class ParentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parents = Parents::with('posyandu')->latest()->get();

        return view('parent.index', ['parents' => $parents]);
    }

    public function detail($id)
    {
        $parents = Parents::where('id', $id)->with('posyandu')->first();
        $childs = Child::where('parent_id', $id)->with('parent')->get();
        return view('parent.detail', [
            'parents' => $parents ,
            'childs' => $childs
        ]);
    }

    public function provinces()
    {
        return collect(Indonesia::allProvinces())->sortBy('name')->values();
    }

    public function cities(Request $request)
    {
        try {
            $data = $request->validate([
                'province_id' => ['required', 'numeric']
            ]);
            $province_id = $data['province_id'];
            $province_with_cities = Indonesia::findProvince($province_id, ['cities']);
            $cities = $province_with_cities['cities'];
            return collect($cities)->sortBy('name')->values();
        } catch (\Throwable $th) {
            return [];
        }
    }

    public function districts(Request $request)
    {
        try {
            $data = $request->validate([
                'city_id' => ['required', 'numeric']
            ]);
            $city_id = $data['city_id'];
            $city_with_district = Indonesia::findCity($city_id, ['districts']);
            $districts = $city_with_district['districts'];
            return collect($districts)->sortBy('name')->values();
        } catch (\Throwable $th) {
            return [];
        }
    }

    public function villages(Request $request)
    {
        try {
            $data = $request->validate([
                'district_id' => ['required', 'numeric']
            ]);
            $district_id = $data['district_id'];
            $district_with_village = Indonesia::findDistrict($district_id, ['villages']);
            $villages = $district_with_village['villages'];
            return collect($villages)->sortBy('name')->values();
        } catch (\Throwable $th) {
            return [];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parent.create', [
            'provinces' => $this->provinces(),
            'posyandus' => Posyandu::latest()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'mothersName' => ['required', 'string', 'max:255'],
            'province' => ['required', 'numeric', 'exists:indonesia_provinces,id'],
            'city' => ['required', 'numeric', 'exists:indonesia_cities,id'],
            'district' => ['required', 'numeric', 'exists:indonesia_districts,id'],
            'village' => ['required', 'numeric', 'exists:indonesia_villages,id'],
            'birthPlace' => ['required', 'string', 'max:255'],
            'birthDate' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'digits_between:6,14'],
            'posyandu_id' => ['required', 'numeric', 'exists:posyandus,id'],
        ]);

        $parents = new Parents();
        $parents['name'] = $data['mothersName'];
        $parents['province_id'] = $data['province'];
        $parents['city_id'] = $data['city'];
        $parents['district_id'] = $data['district'];
        $parents['village_id'] = $data['village'];
        $parents['place_of_birth'] = $data['birthPlace'];
        $parents['date_of_birth'] = Carbon::parse($data['birthDate']);
        $parents['address'] = $data['address'];
        $parents['password'] = Hash::make($data['password']);
        $parents['phone'] = $data['phone'];
        $parents['posyandu_id'] = $data['posyandu_id'];
        $parents->save();

        return redirect('/parent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
