<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Indonesia;
use App\Models\Child;
use App\Models\Parents;
use Carbon\Carbon;

class ChildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $child = child::with('parent')->latest()->get();

        return view('child.index', ['childs' => $child]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request['parent']){
            $data['parent_selected'] = Parents::where('id', $request['parent'])->first();
            $district_with_village = Indonesia::findDistrict($data['parent_selected']['district_id'], ['villages']);
            $data['village'] = $district_with_village['villages'];
            $data['parent'] = Parents::get();
        }else{
            $data['parent_selected']['id'] = null;
            $district_with_village = Indonesia::findDistrict(2657, ['villages']);
            $data['village'] = $district_with_village['villages'];
            $data['parent'] = Parents::get();
        }
    
        return view('child.create', ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'parent' => ['required', 'string', 'exists:parents,id'],
            'village' => ['required', 'numeric', 'exists:indonesia_villages,id'],
            'childName' => ['required', 'string', 'max:255'],
            'fatherName' => ['required', 'string', 'max:255'],
            'motherName' => ['required', 'string', 'max:255'],
            'motherNik' => ['required', 'numeric', 'digits:16'],
            'fatherNik' => ['required', 'numeric', 'digits:16'],
            'nik' => ['required', 'numeric', 'digits:16'],
            'birthDate' => ['required', 'string', 'max:255'],
            'birthPlace' => ['required', 'string', 'max:255'],
            'familyCardNumber' => ['required', 'numeric', 'digits:16'],
        ]);

        $child = new Child();
        $child['parent_id'] = $data['parent'];
        $child['village_id'] = $data['village'];
        $child['name'] = $data['childName'];
        $child['father_name'] = $data['fatherName'];
        $child['mother_name'] = $data['motherName'];
        $child['child_nik'] = $data['nik'];
        $child['father_nik'] = $data['fatherNik'];
        $child['mother_nik'] = $data['motherNik'];
        $child['place_Of_birth'] = $data['birthPlace'];
        $child['date_of_birth'] = Carbon::parse($data['birthDate']);
        $child['gender'] = $request['gender'];
        $child['kk'] = $data['familyCardNumber'];
        $child->save();

        return redirect('/child');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $child = Child::where('id', $id)->with('parent', 'vaksin_vitamin', 'kms')->first();

        return view('child.detail', ['child' => $child]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
