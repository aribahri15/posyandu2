<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReportKms;

class ReportKmsController extends Controller
{
    
    public function store(Request $request, $id){
        $data = $request->validate([
            'bb' => ['required', 'string', 'max:255'],
            'tb' => ['required', 'string', 'max:255'],
            'lk' => ['required', 'string', 'max:255'],
            'age' => ['required', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255'],
            'notes' => ['required', 'string', 'max:255'],
        ]);

        $report_kms = new ReportKms();
        $report_kms['child_id'] = $id;
        $report_kms['bb'] = $data['bb'];
        $report_kms['tb'] = $data['tb'];
        $report_kms['lk'] = $data['lk'];
        $report_kms['age'] = $data['age'];
        $report_kms['status'] = $data['status'];
        $report_kms['notes'] = $data['notes'];
        $report_kms->save();

        return redirect()->back();
    }

    public function update(){
        
    }
}
