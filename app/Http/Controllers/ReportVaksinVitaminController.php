<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ReportVaksinVitamin;

class ReportVaksinVitaminController extends Controller
{
    public function store(Request $request, $id){
        $data = $request->validate([
            'category' => ['required', 'string', 'max:255'],
            'type' => ['required', 'string', 'max:255'],
            'notes' => ['required', 'string', 'max:255'],
        ]);

        $report_vaksin_vitamin = new ReportVaksinVitamin();
        $report_vaksin_vitamin['child_id'] = $id;
        $report_vaksin_vitamin['category'] = $data['category'];
        $report_vaksin_vitamin['type'] = $data['type'];
        $report_vaksin_vitamin['notes'] = $data['notes'];
        $report_vaksin_vitamin->save();

        return redirect()->back();
    }

    public function update(){
        
    }
}
