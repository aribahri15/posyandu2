<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravolt\Indonesia\Models\Village;

class Child extends Model
{
    use HasFactory;
    protected $table = 'childs';

    public function village(){
        return $this->belongsTo(Village::class, 'village_id', 'id');
    }

    public function parent(){
        return $this->belongsTo(Parents::class, 'parent_id', 'id');
    }

    public function kms(){
        return $this->hasMany(ReportKms::class, 'child_id', 'id');
    }

    public function vaksin_vitamin(){
        return $this->hasMany(ReportVaksinVitamin::class, 'child_id', 'id');
    }
}
