<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'posyandu_id' => 1,
            'role' => 'Bidan',
            'phone' => '081234567809',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('posyandus')->insert([
            'name' => 'Posyandu Ajibarang Kulon',
            'village_id' => 32057,
            'district_id' => 2657,
            'city_id' => 189,
            'province_id' => 13,
            'pic_name' => 'Pak Bambang',
            'phone' => '081234567809',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
