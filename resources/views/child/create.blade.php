@extends('layouts.navigation')
@section('title', 'Child')
@section('css')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="../../plugins/datepicker/datepicker3.css">
<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Child
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/child">Child</a></li>
            <li class="active">Create</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Create Child</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- form start -->
                        <form role="form" action="{{ route('child.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="village">Pilih Desa</label>
                                <select class="form-control" id="village" name="village">
                                    @foreach ($data['village'] as $village)
                                    <option value="{{ $village['id'] }}">{{ $village['name'] }}</option>
                                    @endforeach
                                </select>
                                @error('village')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="parent">Nama Orangtua</label>
                                <select class="form-control" id="parent" name="parent">
                                    @foreach ($data['parent'] as $parent)
                                    <option value="{{ $parent['id'] }}" {{ $data['parent_selected']['id']==$parent['id']
                                        ? 'selected' : '' }}>{{ $parent['name'] }}</option>
                                    @endforeach
                                </select>
                                @error('parent')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="childName">Nama Lengkap Anak</label>
                                <input type="text" class="form-control" id="childName" name="childName"
                                    placeholder="Enter child name">
                                @error('childName')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="birtPlace">Tempat Lahir</label>
                                        <input type="text" class="form-control" id="birthPlace" name="birthPlace"
                                            placeholder="Enter birth place">
                                        @error('birthPlace')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="datepicker">Tanggal Lahir</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker"
                                                name="birthDate">
                                            @error('birthDate')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="radio" name="gender" id="gender" value="male" checked="">
                                    Laki Laki
                                </label>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <label>
                                    <input type="radio" name="gender" id="gender" value="female">
                                    Perempuan
                                </label>@error('gender')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="familyCardNumber">No Kartu Keluarga</label>
                                <input type="text" class="form-control" id="familyCardNumber" name="familyCardNumber"
                                    placeholder="Enter family card number">
                                @error('familyCardNumber')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="nik">NIK Anak</label>
                                <input type="text" class="form-control" id="nik" name="nik" placeholder="Enter nik">
                                @error('nik')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="fatherName">Nama Ayah</label>
                                <input type="text" class="form-control" id="fatherName" name="fatherName"
                                    placeholder="Enter father name">
                                @error('fatherName')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="fatherNik">NIK Ayah</label>
                                <input type="text" class="form-control" id="fatherNik" name="fatherNik"
                                    placeholder="Enter father nik">
                                @error('fatherNik')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="motherName">Nama Ibu</label>
                                <input type="text" class="form-control" id="motherName" name="motherName"
                                    placeholder="Enter mother name">
                                @error('motherName')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="motherNik">NIK Ibu</label>
                                <input type="text" class="form-control" id="motherNik" name="motherNik"
                                    placeholder="Enter mother nik">
                                @error('motherNik')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('js')
<!-- bootstrap datepicker -->
<script src="../../plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });
</script>
@endsection